import racetrack, proj1, sample_probs, sample_heuristics
# prob = sample_probs.rectwall32 #Change this to run other tests
problems=[sample_probs.pdes30, sample_probs.rect20b, sample_probs.rect20c, sample_probs.rect20d, sample_probs.rect50, sample_probs.wall8a, sample_probs.wall8b, sample_probs.lhook16, sample_probs.rhook16a, sample_probs.rhook16b, sample_probs.spiral16, sample_probs.spiral24, sample_probs.rectwall8, sample_probs.rectwall16, sample_probs.rectwall32, sample_probs.rectwall32a, sample_probs.walls32]
for prob in problems:
   start = prob[0]
   goal = proj1.edge_to_points(prob[1])
   walls = prob[2]
   print(goal)
   path = proj1.jps(goal, start, walls)
   result = racetrack.main((start, prob[1], walls),'a*', proj1.h_p1, 0, 1)