# We pledge on our honor that we have not given or received any unauthorized
# assistance on this project.
# Alexander Honer (ahoner@umd.edu)
# Stephen Kozak (Skozak1@terpmail.umd.edu)
# Mingyu	Yao (yaomingyu.umd@gmail.com)

import racetrack, math, itertools, heapq

class FastPriorityQueue():
    """
    Because I don't need threading, I want a faster queue than queue.PriorityQueue
    Implementation copied from: https://docs.python.org/3.3/library/heapq.html
    """
    def __init__(self):
        self.pq = []                         # list of entries arranged in a heap
        self.counter = itertools.count()     # unique sequence count

    def add_task(self, task, priority=0):
        'Add a new task'
        count = next(self.counter)
        entry = [priority, count, task]
        heapq.heappush(self.pq, entry)

    def pop_task(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heapq.heappop(self.pq)
            return task
        raise KeyError('pop from an empty priority queue')

    def empty(self):
        return len(self.pq) == 0

def jps(goal, start, walls):

    class PathFound(Exception):
        pass

    def queue_jumppoint(state):
        if state is not None:
            x,y = state
            fx, fy = edge_to_points(goal)[0]
            frontier.add_task((x,y), costField[sx][sy] + max(abs(x - fx), abs(y - fy)))

    # Explore diagonally following a given momentum. Expand Horizontal and Vertical
    # arms out to prob for jump points
    def _jps_explore_dag(state):
        ((sx, sy), (u, v)) = state
        (x1, y1) = (sx, sy)
        currentCost = costField[sx][sy]
        while True:
            (x1, y1) = (x1 + u, y1 + v)
            currentCost += 1
            if racetrack.crash([(x1,y1), (x1 - u ,y1 - v)], walls):
                return None
            elif (x1, y1) in goal:
                costField[x1][y1] = currentCost
                pred[x1][y1] = (sx,sy)
                raise PathFound()
            else:
                if not _has_pred(pred, (x1,y1)):
                    costField[x1][y1] = currentCost
                    pred[x1][y1] = (sx,sy)

            if (racetrack.crash([(x1, y1), (x1 + u, y1)], walls) and
                not(racetrack.crash([(x1, y1), (x1 + u, y1 + v)], walls))):
                return (x1, y1)
            else:
                queue_jumppoint(_jps_explore_str(((x1, y1), (u, 0))))

            if (racetrack.crash([(x1, y1), (x1, y1 + v)], walls) and
                not(racetrack.crash([(x1, y1), (x1 + u, y1 + v)], walls))):
                return (x1, y1)
            else:
                queue_jumppoint(_jps_explore_str(((x1, y1), (0, v))))

    # Explore in straight lines attempting to identify jump points (corners of walls)
    def _jps_explore_str(state):
        ((sx, sy), (u, v)) = state
        (x1, y1) = (sx, sy)
        currentCost = costField[x1][y1]
        while True:
            if racetrack.crash([(x1,y1), (x1 + u,y1 + v)], walls):
                return None
            elif (x1 + u, y1 + v) in goal:
                costField[x1 + u][y1 + v] = currentCost + 1
                pred[x1 + u][y1 + v] = (sx,sy)
                raise PathFound()
            else:
                if not _has_pred(pred, (x1 + u,y1 + v)):
                    costField[x1 + u][y1 + v] = currentCost + 1
                    pred[x1 + u][y1 + v] = (sx,sy)

            if u == 0:
                if (racetrack.crash([(x1, y1), (x1 + 1, y1)], walls) and 
                    not(racetrack.crash([(x1, y1), (x1 + 1, y1 + v)], walls))):
                    if (racetrack.crash([(x1, y1), (x1 - 1, y1)], walls)):
                        if not _has_pred(pred, (x1 + u,y1 + v)):
                            pred[x1 + u][y1 + v] = (sx, sy)

                        queue_jumppoint((x1 + u, y1 + v))
                    return (x1, y1)

                if (racetrack.crash([(x1, y1), (x1 - 1, y1)], walls) and 
                    not(racetrack.crash([(x1, y1), (x1 - 1, y1 + v)], walls))):
                    if (racetrack.crash([(x1, y1), (x1 + 1, y1)], walls)):
                        if not _has_pred(pred, (x1 + u,y1 + v)):
                            pred[x1 + u][y1 + v] = (sx, sy)

                        queue_jumppoint((x1 + u, y1 + v))
                    return (x1, y1)

            elif v == 0:
                if (racetrack.crash([(x1, y1), (x1, y1 + 1)], walls) and 
                    not(racetrack.crash([(x1, y1), (x1 + u, y1 + 1)], walls))):
                    if (racetrack.crash([(x1, y1), (x1, y1 - 1)], walls)):
                        if not _has_pred(pred, (x1 + u,y1 + v)):
                            pred[x1 + u][y1 + v] = (sx, sy)
                        queue_jumppoint((x1 + u, y1 + v))
                    return (x1, y1)

                if (racetrack.crash([(x1, y1), (x1, y1 - 1)], walls) and 
                    not(racetrack.crash([(x1, y1), (x1 + u, y1 - 1)], walls))):
                    if (racetrack.crash([(x1, y1), (x1, y1 + 1)], walls)):
                        if not _has_pred(pred, (x1 + u,y1 + v)):
                            pred[x1 + u][y1 + v] = (sx, sy)

                        queue_jumppoint((x1 + u, y1 + v))
                    return (x1, y1)

            (x1, y1) = (x1 + u, y1 + v)

    def generate_pred(walls):
        points = [point for sublist in walls for tuple in sublist for point in tuple]
        return [[(None, None) for i in range(max(points) + 1)] for j in range(max(points) + 1)]

    frontier = FastPriorityQueue()
    visited = []
    pred = generate_pred(walls)
    costField = generate_pred(walls)
    sx, sy = start
    costField[sx][sy] = 0
    pred[sx][sy] = (sx,sy)

    queue_jumppoint((sx, sy))

    while (not frontier.empty()):
        x, y = frontier.pop_task()
        if not ((x,y) in visited):
            try:
                queue_jumppoint(_jps_explore_str(((x, y), (1, 0))))
                queue_jumppoint(_jps_explore_str(((x, y), (0, 1))))
                queue_jumppoint(_jps_explore_str(((x, y), (-1, 0))))
                queue_jumppoint(_jps_explore_str(((x, y), (0, -1))))

                queue_jumppoint(_jps_explore_dag(((x, y), (1, 1))))
                queue_jumppoint(_jps_explore_dag(((x, y), (1, -1))))
                queue_jumppoint(_jps_explore_dag(((x, y), (-1, -1))))
                queue_jumppoint(_jps_explore_dag(((x, y), (-1, 1))))
            except PathFound:
                # Filter out points on the goal that are not in a final path
                goal_points = list(filter(lambda x: _has_pred(pred, x), goal))
                possible_paths = []
                # Take only the shortest path found and return it
                for point in goal_points:
                    possible_paths.append(edges_to_points(_backtrack(pred, start, point)))
                return min(possible_paths)
        visited.append((x,y))

    raise ValueError("No Path is Found")

# Checks if something has a predecessor
def _has_pred(pred, point):
    (x, y) = point
    sx, sy = pred[x][y]
    return not sx is None

# Helps with backtracking	
def _backtrack(pred, start, goal):
    x1, y1 = goal
    sx, sy = start
    path = []
    while sx != x1 or sy != y1:
        path.append((x1,y1))
        x1, y1 = pred[x1][y1]
    path.reverse()
    return [(start)] + path

# Given an array of a bunch of sequential points, this will return the end nodes
def edges_to_points(edges):
    i = 0
    points = []
    while i < len(edges) - 1:
        points += edge_to_points((edges[i], edges[i+1]))
        i += 1
    return points

# returns an array containing all points between two nodes inclusive
def edge_to_points(edge):
    (x1, y1) = edge[0]
    (x2, y2) = edge[1]
    points = [(x2, y2)]
    dx = x2 - x1
    dy = y2 - y1
    while dx != 0 or dy != 0:
        if dx > 0:
            dx -= 1
        elif dx < 0:
            dx += 1
        if dy > 0:
            dy -= 1
        elif dy < 0:
            dy += 1
        points.append((x1 + dx, y1 + dy))
    points.reverse()
    return points

def sumnum(num):
   if(num <=0):
      return 0
   return sumnum(num-1)+num

def h_p1(state, FLine, walls):
	(x, y), (u, v) = state
	goal = edges_to_points(FLine)
	global OP
	if not('OP' in globals()):
		OP = None

	if (OP == None):
		OP = jps(goal, state[0], walls)
	G_Val = len(jps(goal, state[0], walls))
	OP_Val = len(jps(OP, state[0], walls))

	#m = math.sqrt(u**2+v**2)
	#stop_dist = m*(m-1)/2.0
	
	truex = u 
	truey = v
	
	if truex < 0:
		truex *= -1
	
	if truey < 0:
		truey *= -1
		
	ms = sumnum(max(truex, truey))
	
	if(ms > G_Val+1):
		return 100000
		
	
	if (x,y) in goal and u == 0 and v == 0:
		return -1

	return G_Val + OP_Val