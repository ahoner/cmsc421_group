jps (arr[goal nodes], [x,y], walls)
	should get optimal distance at 1 tile move speed from a starting point to a goal node
	returns arr[path]


calc_dist_f_OP (   arr[goal nodes (arr[path])], dims_of_map,   walls   )
	uses jps for every node in the map, it runs length_array over the return of jps
	this is all compiled into an array
	

calc_dist_f_goal ( arr[goal nodes],  dims_of_map,   walls)
	runs jps with true goal over every node in map, runs length_array over return of jps
	this is also compiled into an array



create_weighted_plain(    arr[goal nodes],   starting_node,     dims_of_map,    walls)
	runs jsp with goal, gets optimal path
	runs calc_dist_f_OP   over return of jps with goal, gets a double array called OPAR
	runs calc_dist_f_goal over every node in map with true goal, values put into TGAR
	WPAR = OPAR + TGAR 
  	
	run the H func over WPAR => true best path!

