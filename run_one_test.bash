#!/bin/bash

###############################################################################
# Author: Dana Nau <nau@cs.umd.edu>, Sept 13, 2017
#
# This bash script takes three arguments -- a racetrack problem,
# a heuristic function, and a search strategy -- runs gsr.search on them.
#
# You can specify two additional optional arguments:
# the amount of verbosity, whether or not to draw a graph.
#
# Here are a few examples you can try:
#   ./run_one_test.bash rect20d h_msdist gbf
#   ./run_one_test.bash rect50 h_esdist gbf
#   ./run_one_test.bash rect50 h_moves gbf
#   ./run_one_test.bash lhook16 h_esdist gbf


set -f    # disable globbing, because we don't want the name a* to be expanded


###############################################################################
### START OF CUSTOMIZATION OPTIONS

### IMPORTANT: Change this to use the correct pathname!
#
python=/usr/local/bin/python3    # python pathname


### Customize these to use the rootnames of the files containing
### your test problems and heuristic functions
#
pfile=sample_probs              # rootname of the file of test problems
hfile=sample_heuristics         # rootname of the file of heuristic functions


### End of customization options
#####################################################################


if [ "$1" == '-help' ] || [ "$1" == '-h' ] || [ $# -lt 3 ] || [ $# -gt 5 ]; then
	echo 'Error, incorrect arguments:' $*
	echo "Proper usage: $0 <prob> <heur> <strat> [verbose] [draw]"
	exit 
fi

prob=$1
heur=$2
strat=$3
verb=2
draw=1
if [ $# -ge 4 ]; then
	verb=$4
fi
if [ $# -ge 5 ]; then
	draw=$5
fi


# first, just run the program and print whatever output the user specified
echo ''
echo "Running '$strat, $heur, $prob"

# create character strings containing the python commands
line1="import racetrack, $pfile, $hfile"
line2="result=racetrack.main($pfile.$prob, '$strat', $hfile.$heur, verbose=$verb, draw=$draw, title='$strat, $heur, $prob')"
line3="print('\nResult ({} states):\n{}'.format(len(result), result))"

$python -c "$line1; $line2; $line3"


# next, do a time test
echo ''
echo "Time test of '$strat, $heur, $prob'"

# To do a proper time test, we don't want python to print anything.
# So we need verbose=0 and draw=0 below.
setup="import racetrack, $pfile, $hfile"
test="racetrack.main($pfile.$prob, '$strat', $hfile.$heur, verbose=0, draw=0)"

$python -m timeit -s "$setup" "$test"
