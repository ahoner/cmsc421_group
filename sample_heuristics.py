"""
Example heuristic functions for Project 1 -- Dana Nau, Sept. 13, 2017

 Each heuristic function takes three arguments: state, edge, walls
   state is the current state. It should have the form ((x,y), (u,v))
   edge is the finish line. It should have the form ((x1,y1), (x2,y2))
   walls is a list of walls, each wall having the form ((x1,y1), (x2,y2))
"""

import math				# one of the heuristic functions takes the square root


def h_edist(state, edge, walls):
	"""Euclidean distance from state to edge, ignoring walls."""
	(x,y) = state[0]
	((x1,y1),(x2,y2)) = edge
	
	# find the smallest and largest coordinates
	xmin = min(x1,x2); xmax = max(x1,x2)
	ymin = min(y1,y2); ymax = max(y1,y2)

	return min([math.sqrt((xx-x)**2 + (yy-y)**2)
		for xx in range(xmin,xmax+1) for yy in range(ymin,ymax+1)])


def h_esdist(state, fline, walls):
	"""
	h_edist modified to include an estimate of how long it will take to stop.
	"""
	((x,y),(u,v)) = state
	m = math.sqrt(u**2+v**2)
	stop_dist = m*(m-1)/2.0
	return max(h_edist(state, fline, walls)+stop_dist/10.0,stop_dist)


def h_mdist(state, edge, walls):
	"""max of x-distance and y-distance from state to edge, ignoring walls."""
	(x,y) = state[0]
	((x1,y1),(x2,y2)) = edge
	
	# find the smallest x distance
	xmin = min(x1,x2)
	xmax = max(x1,x2)
	xdist = min([abs(z-x) for z in range(xmin,xmax+1)])
	
	# find the smallest y distance
	ymin = min(y1,y2)
	ymax = max(y1,y2)
	ydist = min([abs(z-y) for z in range(ymin,ymax+1)])
	
	return max(xdist,ydist)


def h_msdist(state, fline, walls):
	"""
	h_mdist modified to include an estimate of how long it will take to stop.
	"""
	((x,y),(u,v)) = state
	m = max(abs(u),abs(v))
	stop_dist = m*(m-1)/2.0
	return max(h_mdist(state, fline, walls)+m/10.0,m)


def h_moves(state, edge, walls):
	"""
	Try to approximate how many x-moves and y-moves it will take to reach
	the finish line and stop -- or stop and back up, if we're going too fast.
	Some of my formulas aren't quite right, but I doubt it matters much.
	"""
	((x,y), (u,v)) = state
	((x1,y1), (x2,y2)) = edge
	
	# find the minimum x-distance from x to the edge
	x3 = min(x1,x2)
	x4 = max(x1,x2)
	if x < x3:				# case 1: x < smallest endpoint
		xdist = x3 - x
	elif x4 < x:			# case 2: x > largest endpoint
		xdist = x-x4
		x = -x; u = -u		# take mirror image, to make x < smallest endpoint
	else:					# case 3: x is within the endpoints
		xdist = 0
	# Compute how many x-moves to get there and stop (or stop and back up).
	# This is approximate, because I ignored some things that could affect the
	# answer. I might also have made some minor errors.
	if u <= 0:			    # need to come to a stop, and back up
		u = -u
		xmoves = u + 2*math.sqrt(xdist + u*(u-1)/2.0)
	elif u*(u-1)/2 >= xdist:	# going too fast, so overshoot and go back
		xmoves = u + 2*math.sqrt(u*(u-1)/2.0 - xdist)
	else:					    # going slowly enough that we can stop
		xmoves = max(u, -u + 2*math.sqrt(xdist - u*(u-1)/2.0))
	
	# now do the same thing for y
	# find the minimum y-distance from y to the edge
	y3 = min(y1,y2)
	y4 = max(y1,y2)
	if y < y3:				# case 1: y < smallest endpoint
		ydist = y3 - y
	elif y4 < y:			# case 2: y > largest endpoint
		ydist = y-y4
		y = -y; v = -v		# take mirror image, to make y < smallest endpoint
	else:					# case 3: y is within the endpoints
		ydist = 0
	# Compute how many y-moves to get there and stop (or stop and back up).
	# This is approximate, because I ignored some things that could affect the
	# answer. I might also have made some minor errors.
	if v <= 0:			    # need to come to a stop, and back up
		v = -v
		ymoves = v + 2*math.sqrt(ydist + v*(v-1)/2.0)
	elif v*(v-1)/2 >= ydist:	# going too fast, overshoot and go back
		ymoves = v + 2*math.sqrt(v*(v-1)/2.0 - ydist)
	else:					    # going slowly enough that we can stop
		ymoves = max(v, -v + 2*math.sqrt(ydist - v*(v-1)/2.0))
		
	# need to move in both the x and y directions, so take the max
	return max(xmoves,ymoves)

